import React from 'react'
import { render, fireEvent, cleanup} from '@testing-library/react';
import QuestionsPop from './QuestionsPop';

it("renders correctly", ()=>{
    const {queryByTestId} = render(<QuestionsPop/>)
    expect(queryByTestId("button-trigger")).toBeTruthy();
   
})

it("captures open click", done => {
    const {queryByTestId} = render(<QuestionsPop/>)
    const btn = queryByTestId("button-trigger");
    function handleClick() {
      done();
    }
    const { getByText } = render(
      <btn onClick={handleClick}>Click Me</btn>
    );
    const node = getByText("Click Me");
    fireEvent.click(node);    
  });
  afterEach(cleanup);




