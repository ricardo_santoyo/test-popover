import React from "react";

import {  Button, Popover } from "@material-ui/core";
import PopContent from "./PopContent";

const QuestionsPop = (props) => {
    const divRef = React.useRef(null);
    const [open, setOpen] = React.useState(false);
    return (
        <div>
            <Button data-testid="button-trigger" ref={divRef} variant="contained" color="default" onClick={() => setOpen(true)}>
                Open Pop Questions
            </Button>
           
            <Popover
                data-testid="popover"
                open={open}
                anchorEl={() => divRef.current}
                onClose={() => setOpen(false)}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}>               
                <PopContent data={props.data} title={props.title} closePop={()=>setOpen(false)} />
            </Popover>
        </div>
    );
};

export default QuestionsPop;
