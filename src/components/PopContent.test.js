import React from "react";
import { render, fireEvent } from "@testing-library/react";

import PopContent from "./PopContent";

it("renders correctly", () => {
    const { queryByTestId } = render(<PopContent />);

    expect(queryByTestId("pop-container")).toBeTruthy();
    expect(queryByTestId("button-close")).toBeTruthy();
    expect(queryByTestId("title")).toBeTruthy();
});

it("captures close Click", (done) => {
    const { queryByTestId } = render(<PopContent />);
    const btn = queryByTestId("button-close");
    function handleClick() {
        done();
    }
    const { getByText } = render(<btn onClick={handleClick}>Click Me</btn>);
    const node = getByText("Click Me");
    fireEvent.click(node);
});

test("calling props title", () => {
    const { getByTestId, rerender } = render(<PopContent title={"1"} />);
    expect(getByTestId("title").textContent).toBe("1");
    rerender(<PopContent title={"2"} />);
    expect(getByTestId("title").textContent).toBe("2");
});

test("calling props data", () => {
    const { getByTestId, rerender } = render(
        <PopContent
            data={[
                {
                    question: "question",
                    answer: "answer",
                },
            ]}
        />
    );
    expect(getByTestId("question").textContent).toBe("question");
    expect(getByTestId('answer').textContent).toBe('answer')
    rerender(<PopContent data={[
        {
            question: "question2",
            answer: "answer2",
        },
    ]} />);
    expect(getByTestId("question").textContent).toBe("question2");
    expect(getByTestId('answer').textContent).toBe('answer2')
});
