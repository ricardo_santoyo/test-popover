import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, Paper, Grid, Divider, IconButton} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
    question: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    questionAnswer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
}));
const PopContent = (props) => {
    const classes = useStyles();

    return (
        <Paper data-testid="pop-container">
            <Box width={550}>
                <Box bgcolor="primary.main" p={2}>
                    <Grid container spacing={1}>
                        <Grid item xs={10} className={classes.question}>
                            <Typography variant="subtitle1" color="initial" data-testid="title">
                                {props.title}
                            </Typography>
                        </Grid>
                        <Grid item xs={2} className={classes.questionAnswer}>
                            <IconButton data-testid="button-close" onClick={() => props.closePop()}>
                                <CloseIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Box>
                <Box  data-testid="pop-content">
                    {props.data &&
                        props.data.map((data) => {
                            return (
                                <Box key={data.question}>
                                    <Box p={2}>
                                        <Grid container spacing={1}>
                                            <Grid item xs={10} className={classes.question}>
                                                <Typography variant="caption" color="initial" data-testid="question">
                                                    {data.question}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={2} className={classes.questionAnswer}>
                                                <Typography variant="caption" color="initial"  data-testid="answer">
                                                    {data.answer}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                    <Divider />
                                </Box>
                            );
                        })}
                </Box>
            </Box>
        </Paper>
    );
};

export default PopContent;
